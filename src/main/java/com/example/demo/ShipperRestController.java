package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.business.Shipper;
import com.example.demo.business.ShipperRepository;


@RestController
@CrossOrigin
public class ShipperRestController {
	@Autowired
	ShipperRepository shipperrepo;
	
	@RequestMapping(method=RequestMethod.GET, value="/shipper/{id}")
	public Shipper getShipper(@PathVariable int id) {
		return shipperrepo.getShipper(id);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/shipper")
	public List<Shipper> getShippers() {
		return shipperrepo.allShippers();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/allshippers")
	public List<Shipper> getShipper() {
		return shipperrepo.allShippers();
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/shipper")
	public Shipper addShipper(@RequestBody Shipper shipper) {
		return shipperrepo.newShipper(shipper);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/shipper")
	public Shipper editShipper(@RequestBody Shipper shipper) {
		return shipperrepo.saveShipper(shipper);
	}
	
	//Exercise implement and test the delete
	@RequestMapping(method=RequestMethod.DELETE, value="/shipper/{id}")
	public int deleteShipper(@PathVariable int id) {
		return shipperrepo.deleteShipper(id);
	}

}

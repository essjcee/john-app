package com.example.demo.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.business.*;

@Repository
public class SpringDataShippers implements ShipperData{
	//http://codepad.org/Z6WMr7Xm
	@Autowired
    JdbcTemplate template;

	@Override
	public List<Shipper> allShippers() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Shippers";
		return template.query(sql, new ShipperRowMapper());
	}

	@Override
	public Shipper getShipperById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Shippers WHERE ShipperID=?";
        return template.queryForObject(sql, new ShipperRowMapper()
        ,id);
	}

	@Override
	public Shipper editShipper(Shipper s) {
		// TODO Auto-generated method stub
		String sql = "UPDATE Shippers SET CompanyName = ?, Phone = ? " +
				"WHERE ShipperID = ?";
		template.update(sql,s.getName(),s.getPhone(),s.getId());
		return s;
	}

	@Override
	public int deleteShipper(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM Shippers WHERE ShipperID = ?";
		template.update(sql,id);
		return id;
	}

	@Override
	public Shipper addShipper(Shipper s) {
		String sql = "INSERT INTO Shippers(CompanyName, Phone) " +
				"VALUES(?,?)";
		template.update(sql,s.getName(),s.getPhone());
		return s;
	}

}

class ShipperRowMapper implements RowMapper<Shipper>{

	@Override
	public Shipper mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Shipper(rs.getInt("ShipperID"),
				rs.getString("CompanyName"),
				rs.getString("Phone"));
	}
	
}
